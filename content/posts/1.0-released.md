---
title: "Version 1.0-SNAPSHOT Released!"
date: 2019-04-09T12:00:00+02:00
draft: false
---

**The first version of WebCore (1.0-SNAPSHOT) has been
released and is now available for public use.**

You can get it [here](https://nexus.districraft.org/#browse/browse:maven-snapshots:org%2Fdistricraft%2Fwebcore%2F1.0-SNAPSHOT)
or add it to your Maven or Gradle dep file by following
[this tutorial](/use).

Please note this is an Alpha Preview release and absolutely
everything is subject to change. Proceed at your own risk
of having to rewrite a singnificant portion of your project
in the near future...