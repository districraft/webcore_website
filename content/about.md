---
title: "About WebCore"
draft: false
---
**WebCore is an 
[Embedded Jetty](https://www.eclipse.org/jetty/documentation/9.4.x/embedding-jetty.html)
based web application microframework intended for use 
inside web-enabled SpongeMC plugins such as voting systems,
web auctions and markets etc.**

WebCore is not a Sponge plugin in itself and is inteded to
be used as a library for such plugins. It can also be used
outside the SpongeMC ecosystem for pretty much anything, however,
it is strongly aimed towards the Sponge plugin use and when used
with something else, a lot of it's potential will remain
untapped.

Interested? [Use WebCore in your project!](/use)

## What's it good for?
As mentioned before, *WebCore* is a web microframework
with an embedded web server (Jetty) intended for use within
plugins for the SpongeMC Minecraft Server software.

Interested? [Use WebCore in your project!](/use)

## What can it do?
Currently, the following features are available:

 - Controller and Handler based routing with a super-simple
 DSL.
 - (My/Postgre)SQL integration through the JOOQ integration baked into WebCore.
 - Basic authentication with optional web to in-game user integration.
 
We also have the following features on the roadmap:
 
 - Serving static files
 - Better integration with JOOQ
 - More centralized routing
 - Advanced HTTP
 - WebSocket support
 
Interested? [Use WebCore in your project!](/use)

## Who uses it?
Currently, WebCore is used in the **WebAuction** plugin by DistriCraft _(coming soon)_
and a few other interesting projects that are coming up.

Want to extend this list? [Use WebCore in your project!](/use)

## And the license?
The whole **WebCore** codebase is published under the MIT license and can be used by pretty much anyone.

The **WebCore** name, logos and other "marketing materials" are Copyrighted by DistriCraft.

This website is published under a Creative Commons license *CC BY-NC-SA 4.0*.