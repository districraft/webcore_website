---
title: "Use WebCore, Part 2"
draft: false
---
## 2 Integrating WebCore into your code
The first thing you're going to need to do is import the
appropriate packages and get an instance of the WebCore class.

_(There will be a complete code example at the end)_


### 2.1 Initializing WebCore
```java
package com.example.webcoredemo;

import org.districraft.webcore.WebCore;

public class PluginClass {
    // Don't forget to get your Logger from Sponge
    @Inject
    private Logger log;
    
    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        // Initialize WebCore and pass it your logger
        WebCore wc = new WebCore(log);
    }
}
```

### 2.2 Creating the Routing file
WebCore uses it's own "DSL" to define routes, however, you will still need
a little bit of code bling around it to make it work.

Create a new file named ```Routes.java``` and put the following in it:

```java
package com.example.webcoredemo;

import org.districraft.webcore.util.MappingBuilder;
import java.util.Map;
import static org.districraft.webcore.util.MappingBuilder.route;

public class Routes {
    Routes() {
        // Every route is registered here using the route static method.
        route("/", HomeController.class); // Tell WebCore to map the "/" route to the HomeController class.
        // Don't worry, we'll create the HomeController in the next step.
    }

    // This needs to be here so that WebCore knows what to do with your routes. We will be removing this
    // in the future.
    Map<String, Class<?>> build() {
        return MappingBuilder.getAll();
    }
}
```

### 2.3 Creating a Controller
A controller is a class that houses multiple route handlers. Usually, one controller per "resource" is used.
e.g. one for /users, one for /posts, and one for /admin, etc...

Let's create a ```HomeController.java``` file and put some boilerplate code in it.

```java
package com.example.webcoredemo;

import org.districraft.webcore.annotation.Route;
import org.districraft.webcore.http.Controller;
import org.districraft.webcore.http.Response;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class HomeController extends Controller {
    // The @Route annotation tells WebCore that this resource should be available at "/".
    // This is the path AFTER the controller's base path defined in your Routes file.
    @Route("/")
    public void index(HttpServletRequest req, Response res) throws IOException {
        // We use the renderJson from the Response wrapper class to effortlessly render a JSON document.
        res.renderJson("Hello World");
    }
}
```

### 2.4 Setting up MySQL
WebCore needs MySQL or PostgreSQL\* to function properly. 

**Make sure you have MySQL installed and then
create a database named "webcore" and a user of the same name with access rights to the DB.**

The option to use WebCore without a database will be added in the future.


_\* PostgreSQL is not yet supported_

### 2.5 Now for the config
WebCore needs a little bit of configuration before it's started. But just a little bit, promise.

Create a ```webcore.properties``` file somewhere on your classpath and put the following inside:

```properties
# The MySQL Server hostname
mysql_host=localhost
# The MySQL Server por. You can usually keep this at the default value
mysql_port=3306
# The MySQL database you've created
mysql_db=webcore
# The appropriate MySQL User
mysql_user=webcore
# And the User's password
mysql_pass=secret_password

# This is the port at which WebCore's HTTP server will be running.
web_port=8090
```

### 2.6 Starting the WebCore server!
That's it, really! We can now go ahead, launch the server and check out our shiny Hello World page.

Go back to your main class and add this little bit of code:

```java
// We're simply telling WebCore where our Routes are defined and then starting the server.
wc.start(new Routes().build());
```

**You can now go to http://localhost:8090 and you should see the message "Hello World"**

### Congratulations! You've successfuly integrated WebCore into your project!

#### For more info, check out the [JavaDoc](/javadoc/)