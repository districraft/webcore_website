# Welcome to WebCore
*WebCore is a Jetty-based web microframework for web-enbled
SpongeMC plugins.*

### Links:
 - [Learn More about WebCore](/about)
 - [Check Out The Documentation](https://districraft.atlassian.net/wiki/spaces/WCR/overview)
 - [See The ApiDocs](/javadoc)
 - [Find Out What's New in WebCore](/posts)
 - [Use WebCore In Your Project](/use)
 - [Check Out The Code](https://bitbucket.org/districraft/webcore)