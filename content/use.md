---
title: "Use WebCore"
draft: false
---
Using WebCore is very simple. Just add the dependency to your
```pom.xml```, ```build.gradle``` or ```build.sbt``` file, run the respective
install command and then follow this simple guide to get
started with WebCore.

## 1 The WebCore dependency
WebCore is currently not hosted on the Central Maven repository,
so you will need to add the DistriCraft Nexus repo to your
dependency file.

### 1.1 Adding the repo
#### 1.1.1 Maven

```xml
<repositories>
    <repository>
        <id>dc-nexus</id>
        <url>https://nexus.districraft.org/repository/maven-public/</url>
    </repository>
</repositories>
```

#### 1.1.2 Gradle
```groovy
repositories {
    maven {
      url 'https://nexus.districraft.org/repository/maven-public/'
    }
}
```

#### 1.1.3 SBT
Create a ```repositories``` file inside your project's  ```.sbt``` directory with the following contents:
```ini
[repositories]
  dc-maven-snapshots: https://nexus.districraft.org/repository/maven-public/
```

### 1.2 Adding the dependency
_(WebCore uses Maven and we also support Gradle but feel free to use whichever package manager you like that can
use Maven repositories. JAR packages will also be available for download.)_
#### 1.2.1 Maven
```xml
<dependencies>
    <dependency>
        <groupId>org.districraft</groupId>
        <artifactId>webcore</artifactId>
        <version>1.0-SNAPSHOT</version>    
    </dependency>
</dependencies>
```

#### 1.2.2 Gradle
```groovy
compile "org.districraft:webcore:1.0-SNAPSHOT"
```

#### 1.2.3 SBT
```scala
libraryDependencies += "org.districraft" % "webcore" % "1.0-SNAPSHOT"
```

**You will also need Log4j and SLF4j on your classpath, however, if using WebCore inside a SpongeMC plugin,
this has already been takes care of for you.**

## [And there we go! You can now continue to the second part of the tutorial.](/use-2)